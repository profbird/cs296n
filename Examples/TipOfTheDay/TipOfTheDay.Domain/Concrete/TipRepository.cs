﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TipOfTheDay.Domain.Entities;

namespace TipOfTheDay.Domain.Concrete
{

    // These are just stubs that return empty collections or objects for now
    public class TipRepository : ITipRepository
    {
        // Stub
        public IQueryable<Tip> GetTips()
        {
            return (IQueryable<Tip>)(new List<Tip>());
        }

        // Stub
        public Tip GetTip(DateTime date)
        {
            return new Tip();
        }
    }
}